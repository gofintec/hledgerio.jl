using HLedgerIO
using Documenter

DocMeta.setdocmeta!(HLedgerIO, :DocTestSetup, :(using HLedgerIO); recursive=true)

makedocs(;
    modules=[HLedgerIO],
    authors="Oliver Griebel",
    repo="https://gitlab.com/griebel/HLedgerIO.jl/blob/{commit}{path}#{line}",
    sitename="HLedgerIO.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://griebel.gitlab.io/HLedgerIO.jl",
        edit_link="master",
        assets=String[]
    ),
    pages=[
        "Home" => "index.md",
    ]
)
