[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://gofintec.gitlab.io/hledgerio.jl/dev/)
[![pipeline status](https://gitlab.com/gofintec/HLedgerIO.jl/badges/master/pipeline.svg)](https://gitlab.com/gofintec/HLedgerIO.jl/-/commits/master)
[![coverage report](https://gitlab.com/gofintec/HLedgerIO.jl/badges/master/coverage.svg)](https://gitlab.com/gofintec/HLedgerIO.jl/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)
[![ColPrac: Contributor Guide on Collaborative Practices for Community Packages](https://img.shields.io/badge/ColPrac-Contributor%20Guide-blueviolet)](https://github.com/SciML/ColPrac)

# HLedgerIO

**HLedgerIO converts CSV files to HLedger files.**

## Installation

Install with the Julia package manager [Pkg](https://pkgdocs.julialang.org/), just like any other registered Julia package:

```jl
pkg> add HLedgerIO  # Press ']' to enter the Pkg REPL mode.
```

or

```jl
julia> using Pkg; Pkg.add("HLedgerIO")
```

## Usage


## Known Issues


## Contributing

Contributions are very welcome, as are feature requests and suggestions. Please open an [issue](https://gitlab.com/gofintec/HLedgerIO.jl/-/issues) if you encounter any problems.

## License

The HLedgerIO.jl package is licensed under the [MIT License](LICENSE.md).

## Acknowledgements

- [hledger](https://hledger.org/)

[![Buy Me a Coffe](https://www.buymeacoffee.com/assets/img/custom_images/yellow_img.png)](https://www.buymeacoffee.com/gogriebel)