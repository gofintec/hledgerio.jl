module HLedgerIO


const cHLedgerIO_HOME = dirname(dirname(@__FILE__))
export cHLedgerIO_HOME


"""
    Posting

A posting from a HLedger statement.

# Fields
- `status::String`: a status character (empty, !: pending, or *: cleared) (optional).
- `account_name::String`: The account name of the posting (required).
- `amount::String`: The amount of the posting (optional).
- `unit_price::String`: The unit price of the posting (optional).
- `total_price::String`: The total price of the posting (optional).
- `comment::String`: The comment of the posting (optional).
"""
struct Posting
    status::String
    account_name::String
    amount::String
    unit_price::String
    total_price::String
    comment::String

    function Posting(
        account_name::String;
        status::String="",
        amount::String="",
        unit_price::String="",
        total_price::String="",
        comment::String=""
    )
        new(status, accoune_name, amount, unit_price, total_price, comment)
    end
end


"""
    Transaction

A transaction from a HLedger statement.

# Fields
- `date::String`: The date of the transaction.
- `status::String`: A status character (empty, !: pending, or *: cleared) (optional).
- `code::String`: A code (any short number or text, enclosed in parentheses) (optional).
- `description_payee::String`: The payee/payer name of the transaction (optional).
- `description_note::String`: The note of the transaction (optional).
- `comment::String`: The comment of the transaction (optional).
- `postings::Vector{Posting}`: The postings of the transaction (optional).
"""
struct Transaction
    date::String
    status::String
    code::String
    description_payee::String
    description_note::String
    comment::String
    postings::Vector{Posting}

    function Transaction(
        date::String;
        status::String="",
        code::String="",
        description_payee::String="",
        description_note::String="",
        comment::String="",
        postings::Vector{Posting}=[]
    )
        new(date, status, code, description_payee, description_note, comment, postings)
    end
end


include("api/n26io.jl")


end
